﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using MouseKeyboardLibrary;

namespace RightClicker
{
    public partial class ServiceRightClick : ServiceBase
    {
        private KeyboardHook _keyHook = new KeyboardHook();

        public ServiceRightClick()
        {
            InitializeComponent();            
        }

        public void StartService()
        {
            // Запускаем прослушивание
            _keyHook.KeyPress += new System.Windows.Forms.KeyPressEventHandler(_keyHook_KeyPress);
            _keyHook.KeyUp += new System.Windows.Forms.KeyEventHandler(_keyHook_KeyUp);

            _keyHook.Start();
        }

        void _keyHook_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (e.KeyCode == System.Windows.Forms.Keys.RWin)
            {

            }
        }

        void _keyHook_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            char c = e.KeyChar;
        }

        protected override void OnStart(string[] args)
        {
            StartService();
        }

        protected override void OnStop()
        {
            _keyHook.Stop();
        }
    }
}
