﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RightClickerWForms
{
    public class Alphabet
    {
        private List<TransLetter> Letters { get; set; }

        public Alphabet()
        {
            // Инициируем алфавиту
            Letters = new List<TransLetter>()
                {
                    new TransLetter{ RusChar = 'й',EnChar = 'q' },
                    new TransLetter{ RusChar = 'ц',EnChar = 'w' },
                    new TransLetter{ RusChar = 'у',EnChar = 'e' },
                    new TransLetter{ RusChar = 'к',EnChar = 'r' },
                    new TransLetter{ RusChar = 'е',EnChar = 't' },
                    new TransLetter{ RusChar = 'н',EnChar = 'y' },
                    new TransLetter{ RusChar = 'г',EnChar = 'u' },
                    new TransLetter{ RusChar = 'ш',EnChar = 'i' },
                    new TransLetter{ RusChar = 'щ',EnChar = 'o' },
                    new TransLetter{ RusChar = 'з',EnChar = 'p' },
                    new TransLetter{ RusChar = 'х',EnChar = '[' },
                    new TransLetter{ RusChar = 'ъ',EnChar = ']' },
                    new TransLetter{ RusChar = 'э',EnChar = '\'' },
                    new TransLetter{ RusChar = 'ж',EnChar = ';' },
                    new TransLetter{ RusChar = 'д',EnChar = 'l' },
                    new TransLetter{ RusChar = 'л',EnChar = 'k' },
                    new TransLetter{ RusChar = 'о',EnChar = 'j' },
                    new TransLetter{ RusChar = 'р',EnChar = 'h' },
                    new TransLetter{ RusChar = 'п',EnChar = 'g' },
                    new TransLetter{ RusChar = 'а',EnChar = 'f' },
                    new TransLetter{ RusChar = 'в',EnChar = 'd' },
                    new TransLetter{ RusChar = 'ы',EnChar = 's' },
                    new TransLetter{ RusChar = 'ф',EnChar = 'a' },
                    new TransLetter{ RusChar = 'я',EnChar = 'z' },
                    new TransLetter{ RusChar = 'ч',EnChar = 'x' },
                    new TransLetter{ RusChar = 'с',EnChar = 'c' },
                    new TransLetter{ RusChar = 'м',EnChar = 'v' },
                    new TransLetter{ RusChar = 'и',EnChar = 'b' },
                    new TransLetter{ RusChar = 'т',EnChar = 'n' },
                    new TransLetter{ RusChar = 'ь',EnChar = 'm' },
                    new TransLetter{ RusChar = 'б',EnChar = ',' },
                    new TransLetter{ RusChar = 'ю',EnChar = '.' },
                    new TransLetter{ RusChar = '.',EnChar = '/' },
                    new TransLetter{ RusChar = ',',EnChar = '?' },
                    new TransLetter{ RusChar = '!',EnChar = '!' },
                    new TransLetter{ RusChar = '"',EnChar = '@' },
                    new TransLetter{ RusChar = '№',EnChar = '#' },
                    new TransLetter{ RusChar = ';',EnChar = '$' },
                    new TransLetter{ RusChar = '%',EnChar = '%' },
                    new TransLetter{ RusChar = ':',EnChar = '^' },
                    new TransLetter{ RusChar = '?',EnChar = '&' }
                    //new TransLetter{ RusChar = '*',EnChar = '*' },
                    //new TransLetter{ RusChar = '(',EnChar = '(' },
                    //new TransLetter{ RusChar = ')',EnChar = ')' },
                    //new TransLetter{ RusChar = '_',EnChar = '_' },
                    //new TransLetter{ RusChar = '+',EnChar = '+' }
                    
                };

        }

        public string Transcode(string input)
        {
            Dictionary<char,char> changeTable = new Dictionary<char, char>();

            // меняем шило на мыло =)
            StringBuilder result = new StringBuilder();
            foreach (char letter in input.AsEnumerable())
            {
                bool transExists = false;
                // Бежим по тексту и смотрим, надо ли менять
                TransLetter foundLetter = null;
                foundLetter = Letters.FirstOrDefault(l => l.EnChar == char.ToLower(letter));
                if (foundLetter != null)
                {
                    transExists = true;
                    // Формируем таблицу
                    //FormTable(changeTable, letter, foundLetter.RusChar);
                    result.Append(GetRightCase(letter,foundLetter.RusChar));
                }
                else
                {
                    // Ищем в другом алфавите
                    foundLetter = Letters.FirstOrDefault(l => l.RusChar == Char.ToLower(letter));

                    if (foundLetter != null)
                    {
                        transExists = true;
                        // Формируем таблицу
                        //FormTable(changeTable, letter, foundLetter.EnChar); 
                        result.Append(GetRightCase(letter, foundLetter.EnChar));
                    }
                }
                if (!transExists)
                {
                    result.Append(letter);
                }
            }

            // Производим замену
            //foreach (var kv in changeTable)
            //{
            //    result = result.Replace(kv.Key, kv.Value);
            //}

            return result.ToString();
        }

        private static char GetRightCase(char letter, char foundLetter)
        {
                char changeChar = foundLetter;
                if (char.IsUpper(letter))
                {
                    changeChar = char.ToUpper(changeChar);
                }

            return changeChar;
        }
    }
}
