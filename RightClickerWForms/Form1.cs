﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MouseKeyboardLibrary;
namespace RightClickerWForms
{

    public partial class FormDummy : Form
    {
        public FormDummy()
        {
            InitializeComponent();            
        }

        private void FormDummy_Load(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Minimized;            
            this.Width = 0;
            this.Height = 0;
            this.Opacity = 0;
        }

        private void FormDummy_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
                Hide();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }       
    }
}
